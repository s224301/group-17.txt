#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <openssl/sha.h>
#include <sys/socket.h>
#include <unistd.h>
#include "messages.h"



#define MAX_PENDING_CONNECTIONS 10
#define MAX_MESSAGE_SIZE 49

uint64_t reverseHashing(const unsigned char* tHash, uint64_t start, uint64_t end, int difficulty) {
    unsigned char hash[SHA256_DIGEST_LENGTH];
    unsigned char data[8];
    uint64_t temp;

    for (temp = start; temp <= end; temp++) {
        memcpy(data, &temp, sizeof(uint64_t));
        SHA256(data, sizeof(data), hash);

        if (memcmp(hash, tHash, sizeof(hash)) == 0) {
            return temp;
        }
    }

    return UINT64_MAX;
}

void handleClient(int clientSocket) {

    struct PacketRequest clientRequest;
    ssize_t bytesRead = recv(clientSocket, &clientRequest, sizeof(clientRequest), 0);

    if (bytesRead < 0) {
        perror("Error request");
        close(clientSocket);
        return;
    }

    int requestDifficulty = clientRequest.difficulty;

    uint64_t result = reverseHashing(clientRequest.hash, be64toh(clientRequest.start), be64toh(clientRequest.end), requestDifficulty);

    uint64_t hostEndian = htobe64(result);

    struct PacketResponse serverResponse;
    serverResponse.answer = hostEndian;

    printf("Response: %lu\n", serverResponse.answer);

    if (send(clientSocket, &serverResponse.answer, sizeof(serverResponse.answer), 0) < 0) {
        perror("Error sending the response");
    }

    close(clientSocket);
}

int main(int argc, char* argv[]) {
    if (argc != 2) {
        fprintf(stderr, "Usage is following: %s <port>\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    int serverSocket, clientSocket;
    struct sockaddr_in serverAddr, clientAddr;
    socklen_t clientAddrLen = sizeof(clientAddr);

    int PORT = atoi(argv[1]);

    serverSocket = socket(AF_INET, SOCK_STREAM, 0);
    if (serverSocket == -1) {
        perror("Socket creation failed");
        exit(EXIT_FAILURE);
    }

    memset(&serverAddr, 0, sizeof(serverAddr));
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    serverAddr.sin_port = htons(PORT);

    if (bind(serverSocket, (struct sockaddr*)&serverAddr, sizeof(serverAddr)) != 0) {
        perror("Socket bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(serverSocket, MAX_PENDING_CONNECTIONS) != 0) {
        perror("Listen failed");
        exit(EXIT_FAILURE);
    }

    printf("Server listening from port %d...\n", PORT);

    while (1) {
        clientSocket = accept(serverSocket, (struct sockaddr*)&clientAddr, &clientAddrLen);
        if (clientSocket < 0) {
            perror("Server accept failed");
            exit(EXIT_FAILURE);
        }

        printf("Accepted a connection from %s:%d\n", inet_ntoa(clientAddr.sin_addr), ntohs(clientAddr.sin_port));

        handleClient(clientSocket);
    }
}
